//
//  AppDelegate.h
//  p07-hom
//
//  Created by Corey Hom on 4/4/16.
//  Copyright © 2016 Corey Hom. Stanley Chan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


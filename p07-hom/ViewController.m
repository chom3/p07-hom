//
//  ViewController.m
//  p07-hom
//
//  Created by Corey Hom on 4/4/16.
//  Copyright © 2016 Corey Hom. Stanley Chan. All rights reserved.
//

#import "ViewController.h"



@interface spotObject:NSObject
{
    int value;
    int location;
    
}

@property(nonatomic, readwrite) int value;
@property(nonatomic, readwrite) int location;

@end

@implementation spotObject

@synthesize value;
@synthesize location;

-(id)init
{
    self = [super init];
    if (!self) return nil;
    return self;
}
@end


@interface ViewController ()

@end

@implementation ViewController


@synthesize s1,s2,s3,s4,s5,s6,s7,s8,s9;
@synthesize oImg,xImg,blankImg;
@synthesize reset;
@synthesize playerTurn;
@synthesize board;

-(void) viewDidAppear:(BOOL)animated
{
    playerTurn = 1;
    oImg = [UIImage imageNamed:@"tic-tac-toe-O"];
    xImg = [UIImage imageNamed:@"tic-tac-toe-X"];
    blankImg =[UIImage imageNamed:@"blank"];

}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    
    // check to see which UIImage view was touched
    if(CGRectContainsPoint([s1 frame], [touch
                                        locationInView:self.view])){
        if(playerTurn==1 && s1.image == NULL){ s1.image = xImg; }
    }
    
    if(CGRectContainsPoint([s2 frame], [touch
                                        locationInView:self.view])){
        if(playerTurn==1 && s2.image == NULL){ s2.image = xImg; }
    }
    
    if(CGRectContainsPoint([s3 frame], [touch
                                        locationInView:self.view])){
        if(playerTurn==1 && s3.image == NULL){ s3.image = xImg; }
    }
    
    if(CGRectContainsPoint([s4 frame], [touch
                                        locationInView:self.view])){
        if(playerTurn==1 && s4.image == NULL){ s4.image = xImg; }
    }
    
    if(CGRectContainsPoint([s5 frame], [touch
                                        locationInView:self.view])){
        if(playerTurn==1 && s5.image == NULL){ s5.image = xImg; }
    }
    
    
    if(CGRectContainsPoint([s6 frame], [touch
                                        locationInView:self.view])){
        if(playerTurn==1 && s6.image == NULL){ s6.image = xImg; }
    }
    
    if(CGRectContainsPoint([s7 frame], [touch
                                        locationInView:self.view])){
        if(playerTurn==1 && s7.image == NULL){ s7.image = xImg; }
    }
    
    if(CGRectContainsPoint([s8 frame], [touch
                                        locationInView:self.view])){
        if(playerTurn==1&& s8.image == NULL){ s8.image = xImg; }
    }
    
    if(CGRectContainsPoint([s9 frame], [touch
                                        locationInView:self.view])){
        if(playerTurn==1 && s9.image == NULL){ s9.image = xImg; }
    }
    
    if (playerTurn == 1)
    {
        playerTurn = 2;
    }
    else
    {
        playerTurn = 1;
    }
    if (playerTurn == 2)
    {

        int locationsLeft = 0;
        NSMutableArray *currentBoard = [[NSMutableArray alloc]init];
        for(UIImageView* image in board){

            if(image.image == NULL){
                locationsLeft++;
                [currentBoard addObject:blankImg];
            }
            else{
                [currentBoard addObject:image.image];
            }
        }
        
        NSLog(@"Locations left %d", locationsLeft);
        
        spotObject *bestSpot = [[spotObject alloc] init];
        bestSpot = [self minMaxAlgorithm:currentBoard locations:locationsLeft];
        int nextSpot = bestSpot.location;
        NSLog(@"Next spot %d", nextSpot);
        switch (nextSpot) {
            case 0:
                s1.image = oImg;
                break;
            case 1:
                s2.image = oImg;
                break;
            case 2:
                s3.image = oImg;
                break;
            case 3:
                s4.image = oImg;
                break;
            case 4:
                s5.image = oImg;
                break;
            case 5:
                s6.image = oImg;
                break;
            case 6:
                s7.image = oImg;
                break;
            case 7:
                s8.image = oImg;
                break;
            case 8:
                s9.image = oImg;
                break;
            default:
                break;
        }
    }
    playerTurn = 1;
    if([self checkForWin] == TRUE){
        NSLog(@"Winner");
    }
}


-(spotObject*) minMaxAlgorithm: (NSMutableArray*)imageBoard locations:(int)numLocations
{
    NSLog(@"Image board size %lu", imageBoard.count);

    //tells the value of each location
    NSMutableArray *spotValues = [[NSMutableArray alloc] init];
    spotObject *bestSpot = [[spotObject alloc] init];
    int counter = 0;
    for(int i = 0; i < 9; i++){
        UIImage *image = [imageBoard objectAtIndex:i];
        spotObject *currentSpot = [[spotObject alloc] init];
        
        if(playerTurn == 2){
            if(image == blankImg){
                counter++;
                playerTurn = 1;
                NSLog(@"Player 2 statement");
                imageBoard[i] = oImg;
                BOOL winner = [self checkArrayForWin:imageBoard];
                //if we won then we don't need to recurse down
                if(winner == TRUE){
                    currentSpot.value = -10;
                    currentSpot.location = i;
                    [spotValues addObject:currentSpot];
                }
                //else if we didn't win
                else{
                    int remainingLocations = numLocations - 1;
                    //if no more spots left and we didn't win then its a tie
                    if(remainingLocations == 0){
                        currentSpot.value = 0;
                        currentSpot.location = i;
                        [spotValues addObject:currentSpot];
                    }
                    //else we need to recurse down the tree
                    else{
                        NSLog(@"Recursing here");
                        spotObject *newSpot = [[spotObject alloc]init];
                        newSpot = [self minMaxAlgorithm:imageBoard locations:remainingLocations];
                        [spotValues addObject:newSpot];

                    }
                }
            }
        }
        else{
            if(image == blankImg){
                counter++;
                playerTurn = 2;
                NSLog(@"Player 1 statement");
                imageBoard[i] = xImg;
                BOOL winner = [self checkArrayForWin:imageBoard];
                //if we won then we don't need to recurse down
                if(winner == TRUE){
                    currentSpot.value = 10;
                    currentSpot.location = i;
                    [spotValues addObject:currentSpot];
                }
                //else if we didn't win
                else{
                    int remainingLocations = numLocations - 1;
                    //if no more spots left and we didn't win then its a tie
                    if(remainingLocations == 0){
                        currentSpot.value = 0;
                        currentSpot.location = i;
                        [spotValues addObject:currentSpot];
                    }
                    //else we need to recurse down the tree
                    else{
                        NSLog(@"Recursing here");
                        spotObject *newSpot = [[spotObject alloc]init];
                        newSpot = [self minMaxAlgorithm:imageBoard locations:remainingLocations];
                        [spotValues addObject:newSpot];
                        
                    }
                }
            }
        }
    }
    NSLog(@"Spot value size: %lu",spotValues.count);

    //now we must analyze the location values
    int bestChoiceSpot = -1;
    int bestValue = 0;
    
    if(playerTurn == 1){
        playerTurn = 2;
        bestValue = 100;
    }
    else{
        playerTurn = 1;
        bestValue = -100;
    }
    
    for(int i = 0; i < spotValues.count; i++){
        spotObject *mySpot = [spotValues objectAtIndex:i];
        int spotValue = mySpot.value;

        NSLog(@"Value: %d", spotValue);
        if(playerTurn == 2){
            if(spotValue < bestValue){
                bestValue = spotValue;
                bestChoiceSpot = mySpot.location;
            }
        }
        else{
            if(spotValue > bestValue){
                bestValue = spotValue;
                bestChoiceSpot = mySpot.location;
            }
        }
    }
    bestSpot.value = bestValue;
    bestSpot.location = bestChoiceSpot;
    NSLog(@"Counter: %d", counter);
    return bestSpot;

}

-(IBAction) buttonReset{
    [self resetBoard];
}

-(void) resetBoard{
    /// clear the images stored in the UIIMageView
    s1.image = NULL;
    s2.image = NULL;
    s3.image = NULL;
    s4.image = NULL;
    s5.image = NULL;
    s6.image = NULL;
    s7.image = NULL;
    s8.image = NULL;
    s9.image = NULL;
    
    // reset the player and update the label text
    playerTurn= 1;
}

-(BOOL) checkArrayForWin: (NSMutableArray*) currentBoard{
    BOOL didWin = FALSE;
    //DIAGONAL WINS, HORIZONTAL WINS, VERTICAL WINS in that order respectively
    if((currentBoard[0] == currentBoard[4] && currentBoard[4] == currentBoard[8] && currentBoard[0] != blankImg) || (currentBoard[2] == currentBoard[4] && currentBoard[4]== currentBoard[6] && currentBoard[2] != blankImg)){
        didWin = TRUE;
    }
    else if((currentBoard[0] == currentBoard[1] && currentBoard[1] == currentBoard[2] && currentBoard[0] != blankImg) || (currentBoard[3] == currentBoard[4] && currentBoard[4] == currentBoard[5] && currentBoard[3] != blankImg) || (currentBoard[6] == currentBoard[7] && currentBoard[7] == currentBoard[8] && currentBoard[6] != blankImg)){
        didWin = TRUE;
    }
    else if((currentBoard[0] == currentBoard[3] && currentBoard[3] == currentBoard[6] && currentBoard[0] != blankImg) || (currentBoard[1] == currentBoard[4] && currentBoard[4] == currentBoard[7] && currentBoard[1] != blankImg) || (currentBoard[2] == currentBoard[5]  && currentBoard[5] == currentBoard[8] && currentBoard[2] != blankImg)){
        didWin = TRUE;
    }
    return didWin;
}

-(BOOL) checkForWin{
    BOOL didWin = FALSE;
    //DIAGONAL WINS, HORIZONTAL WINS, VERTICAL WINS in that order respectively
    if((s1.image == s5.image && s5.image == s9.image && s5.image != NULL) || (s3.image == s5.image && s5.image == s9.image && s5.image != NULL)){
        didWin = TRUE;
    }
    else if((s1.image == s2.image && s2.image == s3.image && s2.image != NULL) || (s4.image == s5.image && s5.image == s6.image && s5.image != NULL) || (s7.image == s8.image && s8.image == s9.image && s8.image != NULL)){
        didWin = TRUE;
    }
    else if((s1.image == s4.image && s4.image == s7.image && s4.image != NULL) || (s2.image == s5.image && s5.image == s8.image && s5.image != NULL) || (s3.image == s6.image  && s6.image == s9.image && s6.image != NULL)){
        didWin = TRUE;
    }
    return didWin;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  ViewController.h
//  p07-hom
//
//  Created by Corey Hom on 4/4/16.
//  Copyright © 2016 Corey Hom. Stanley Chan. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ViewController : UIViewController{
    IBOutlet UIImage *oImg;
    IBOutlet UIImage *xImg;
    
    IBOutlet UIImageView *s1;
    IBOutlet UIImageView *s2;
    IBOutlet UIImageView *s3;
    IBOutlet UIImageView *s4;
    IBOutlet UIImageView *s5;
    IBOutlet UIImageView *s6;
    IBOutlet UIImageView *s7;
    IBOutlet UIImageView *s8;
    IBOutlet UIImageView *s9;
    
    IBOutlet UIButton *reset;
    

    IBOutletCollection(UIImageView) NSMutableArray *board;

}

@property (nonatomic,retain) UIImage *oImg;
@property (nonatomic,retain) UIImage *xImg;
@property (nonatomic,retain) UIImage *blankImg;


@property int playerTurn;

@property (nonatomic,retain) UIButton *resetButton;

@property (nonatomic,retain) NSMutableArray *board;

@property (nonatomic,retain) UIImageView *s1;
@property (nonatomic,retain) UIImageView *s2;
@property (nonatomic,retain) UIImageView *s3;
@property (nonatomic,retain) UIImageView *s4;
@property (nonatomic,retain) UIImageView *s5;
@property (nonatomic,retain) UIImageView *s6;
@property (nonatomic,retain) UIImageView *s7;
@property (nonatomic,retain) UIImageView *s8;
@property (nonatomic,retain) UIImageView *s9;

@property (nonatomic, retain) UIButton *reset;

-(BOOL) checkForWin;

-(IBAction)buttonReset;



@end

